$(document).ready(setActiveMenu());

function setActiveMenu(){
    var currentUrl = window.location.href;
    var links = document.getElementsByClassName("menuOption");
    
    for(var i = 0; i < links.length;i++){
        if( links[i].href == currentUrl){
            links[i].parentNode.classList.add("active");
        }
    }
}