$(document).ready(function(){

    var table = $("#tabelaUsuario").DataTable({
        rowReorder: {
            selector: 'td:nth-child(2)'
        },
        responsive: true,
        "lengthMenu":[ [5,10,25], [5,10,25] ]
    });

    var dataTable = new Hammer( document.querySelector('#tabelaUsuario') );
    var mainContent = new Hammer( document.querySelector('#mainContent') );

    dataTable.on('swiperight', function(){
        table.page('previous').draw('page');
    });
    dataTable.on('swipeleft', function(){
        table.page('next').draw('page');
    });

    mainContent.on('swipeleft', function(){
        window.location = $('#novoUsuario').attr('href');
    });
});