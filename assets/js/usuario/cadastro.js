$(document).ready(function(){

    var hammer = new Hammer( document.querySelector('.col-sm-10') );

    hammer.on('swiperight', function(){
        history.back(1);
    });
    
    // Campo 'area de atuacao' por padrao esta oculto
    $("#areaAtuacao").hide();

    $('#telefone').mask('00000-0000');

    // Funcao para validacao do formulario utilizando a biblioteca 'Jquery Validate'
    $("#formularioCadastro").validate({
        rules:{
            nome: {
                required: true,
                minlength: 3
            },
            telefone: {
                required: true,
                minlength: 9
            },
            formacao_id: "required"
        },
        messages:{
            nome: {
                required: "Por favor preencha este campo corretamente",
                minlength: "Este campo necessita de no minimo 3 caracteres"
            },
            telefone: {
                required: "Por favor preencha este campo corretamente",
                minlength: "Por favor entre com um numero de telefone valido( 9 digitos)"
            },
            formacao_id: "Por favor selecione uma das opcoes"
            
        }
    });

    validaCampo();
});

// Exibe o campo 'area de atuacao' mediante troca de valor do campo 'formacao'
$("#formacaoSelect").change(function(){
    if( $(this).val() > 3  && $("#areaAtuacao").is(":hidden") ){
        $("#areaAtuacao").toggle("drop");
    }else if( $(this).val() <= 3 && $("#areaAtuacao").is(":visible") ){
        $("#areaAtuacao").toggle("drop");
    }
});

// Botao para salvar usuario
$('#salvaUsuario').on('click',function(){
    $('#telefone').unmask();
    $('#cursoId').css('text-transform','capitalize');
    $('#formularioCadastro').submit();
});

// Exibe o campo de 'area de atuacao' na tela de edicao de usuario
function validaCampo(){
    var campo = $('#formacaoSelect').val();
    if(campo > 3){
        $("#areaAtuacao").show();
    }
}

$('#voltar').on('click',function(){
    history.back(1);
});