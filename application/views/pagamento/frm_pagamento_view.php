<main role="main" class="col-sm-9 ml-sm-auto col-md-10 pt-3">
    <div class="container">
        <div class="row">
            <div class="col-10 ">
                <div class="col-12 pageTitle">
                    <legend><a class="fa fa-arrow-left" href="<?php echo base_url("pagamento")?>" ></a>&nbsp<?php echo $tituloPagina?><a id="salvaPagamento" class="fa fa-floppy-o pull-right" href="#"></a></legend>
                </div>
                <?php
                    echo $this->session->flashdata('statusOperacao');
                    $data = ['id'=>'formularioCadastro'];
                    echo form_open('pagamento/salvaPagamento',$data);
                        echo "<div class='row'>";
                            echo "<div class='col-3'>";
                                echo form_label("Nome","nome");
                                $op = ['name'=>'nome','value'=>$dadosPagamento['nome'],'placeholder'=>'Nome','class'=>"form-control",'id'=>'nome','required'=>''];
                                echo form_input($op);
                            echo "</div>";
                            echo "<div class='col-3'>";
                                echo form_label("Telefone","telefone");
                                $op = ['name'=>'telefone','value'=>$dadosPagamento['telefone'],'placeholder'=>'Telefone','class'=>"form-control",'id'=>'telefone','required'=>''];
                                echo form_input($op);
                            echo "</div>";
                            echo "<div class='col-3'>";
                                echo form_label("Valor","valor");
                                $op = ['name'=>'valor','value'=>$dadosPagamento['valor'],'placeholder'=>'valor','class'=>"form-control"];
                                echo form_input($op);
                            echo "</div>";
                            echo "<div class='col-3'>";
                                echo form_label("Data","data");
                                $op = ['name'=>'data','value'=>$dadosPagamento['data'],'placeholder'=>'data','class'=>"form-control"];
                                echo form_input($op);
                            echo "</div>";
                        echo "</div>";
                    echo form_close();
                ?>
            </div>
        </div>
    </div>
</main>
<script src="<?php echo base_url('assets/js/pagamento/cadastro.js')?>"></script>