<main role="main" class="col-sm-9 ml-sm-auto col-md-10 pt-3">
    <div class="container">
        <div class="row">
            <div class="col-10 ">
                <div class="col-12 pageTitle">
                    <legend >Livros <a class="fa fa-plus pull-right" href="livro/cadastraLivro"></a></legend>
                </div>
                <div class="row">
                    <div class="col-10 offset-1">
                        <table class="table" id="tabelaLivro">
                            <thead>
                                <tr>
                                    <th>Nome</th>
                                    <th>Autor</th>
                                    <th>Ano</th>
                                    <th>Tema</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    foreach($livros as $livro){
                                        echo "<tr>".
                                            // "<td type='hidden'>".$usuario['id']."</td>".
                                            "<td>".$livro['nome']."</td>".
                                            "<td>".$livro['autor']."</td>".
                                            "<td>".$livro['ano']."</td>".
                                            "<td>".$livro['tema']."</td>".
                                            "<td><a class='fa fa-pencil iconeEdicao' href='livro/editarLivro/".$livro['id']."'></a></td>".
                                         "</tr>";
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>    
    </div>
</main>
<script src="<?php echo base_url('assets/js/livro/home.js')?>"></script>