<main role="main" class="col-md-10 offset-md-1 pt-3">
    <div class="container">
        <div class="row">
            <div class="col-10 ">
                <div class="col-12 pageTitle">
                    <legend><a class="fa fa-arrow-left" href="<?php echo base_url("livro")?>" ></a>&nbsp<?php echo $tituloPagina?><a id="salvaLivro" class="fa fa-floppy-o pull-right" href="#"></a></legend>
                </div>
                <?php
                    echo $this->session->flashdata('statusOperacao');
                    $data = ['id'=>'formularioLivro'];
                    echo form_open('livro/salvaLivro',$data);
                        echo "<div class='row'>";
                            echo "<div class='col-3'>";
                                echo form_label("Nome","nome");
                                $op = ['name'=>'nome','value'=>$dadosLivro['nome'],'placeholder'=>'Nome','class'=>"form-control",'id'=>'nome','required'=>''];
                                echo form_input($op);
                            echo "</div>";
                            echo "<div class='col-3'>";
                                echo form_label("Autor","autor");
                                $op = ['name'=>'autor','value'=>$dadosLivro['autor'],'placeholder'=>'Autor','class'=>"form-control",'id'=>'autor','required'=>''];
                                echo form_input($op);
                            echo "</div>";
                            echo "<div class='col-3'>";
                                echo form_label("Ano","ano");
                                $op = ['name'=>'ano','value'=>$dadosLivro['ano'],'placeholder'=>'Ano','class'=>"form-control",'id'=>'ano','required'=>''];
                                echo form_input($op);
                            echo "</div>";
                            echo "<div class='col-3'>";
                                echo form_label("Tema","tema");
                                $op = ['name'=>'tema','value'=>$dadosLivro['tema'],'placeholder'=>'Tema','class'=>"form-control"];
                                echo form_input($op);
                            echo "</div>";
                            
                        echo "</div>";
                    echo form_close();
                ?>
            </div>
        </div>
    </div>
</main>