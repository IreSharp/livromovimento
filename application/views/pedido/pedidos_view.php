<main role="main" class="col-sm-9 ml-sm-auto col-md-10 pt-3">
    <div class="container">
        <div class="row">
            <div class="col-10 ">
                <div class="col-12 pageTitle">
                    <legend >Pedidos <a class="fa fa-plus pull-right" href="pedido/cadastraPedido"></a></legend>
                </div>
                <div class="row">
                    <div class="col-10 offset-1">
                        <table class="table" id="tabelaPedido">
                            <thead>
                                <tr>
                                    <th>Usuario</th>
                                    <th>Livro</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    foreach($pedidos as $pedido){
                                        echo "<tr>".
                                            // "<td type='hidden'>".$usuario['id']."</td>".
                                           // "<td>".$pedido['id']."</td>".
                                            "<td>".$pedido['Usuario']."</td>".
                                            "<td>".$pedido['Livro']."</td>".
                                           // "<td>".$pedido['data_pedido']."</td>".
                                           // "<td>".$pedido['status']."</td>".
                                            "<td><a class='fa fa-pencil iconeEdicao' href='pedido/editarPedido/".$pedido['id']."'></a></td>".
                                         "</tr>";
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>    
    </div>
</main>
<script src="<?php echo base_url('assets/js/pedido/home.js')?>"></script>
<script src="<?php echo base_url('assets/js/jquery.min.js')?>"></script>
<script src="<?php echo base_url('assets/js/JqueryUI/jquery-ui.min.js')?>"></script>