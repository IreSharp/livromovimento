<main role="main" class="col-sm-9 ml-sm-auto col-md-10 pt-3">
    <div class="container">
        <div class="row">
            <div class="col-10 ">
                <div class="col-12 pageTitle">
                    <legend><a class="fa fa-arrow-left" href="<?php echo base_url("pedido")?>" ></a>&nbsp<?php echo $tituloPagina?><a id="editaPedido" class="fa fa-floppy-o pull-right" href="#"></a></legend>
                </div>
                <?php
                    echo $this->session->flashdata('statusOperacao');
                    $data = ['id'=>'formularioPedido'];
                    echo form_open('pedido/salvaPedido',$data);
                        echo "<div class='row'>";
                            echo "<div class='col-4'>";
                            echo "<input type='hidden' name='id' value=".$dadosPedido['id'].">";
                                echo form_label("Usuario","usuario");
                                $op = ['name'=>'usuario','value'=>$dadosPedido['Usuario'],'placeholder'=>'Nome do usuario','class'=>"form-control",'id'=>'usuario','required'=>''];
                                echo form_input($op);
                            echo "</div>";
                            echo "<div class='col-4'>";
                                echo form_label("Livro","livro");
                                $op = ['name'=>'livro','value'=>$dadosPedido['Livro'],'placeholder'=>'Nome do livro','class'=>"form-control",'id'=>'livro','required'=>''];
                                echo form_input($op);
                            echo "</div>";
                            //echo "<div class='col-3'>";
                              //  echo form_label("Ano","ano");
                               // $op = ['name'=>'ano','value'=>$dadosPedido['Data'],'placeholder'=>'Ano','class'=>"form-control",'id'=>'ano','required'=>''];
                                //echo form_input($op);
                            //echo "</div>";
                            //echo "<div class='col-4'>";
                              //  echo form_label("Stat","tema");
                               /// $op = ['name'=>'tema','value'=>$dadosPedido['Status'],'placeholder'=>'Tema','class'=>"form-control"];
                               // echo form_input($op);
                           // echo "</div>";
                            echo "<div class='col-4'>";
                                echo '<input type="submit" value="Submit">';
                                echo "</div>";
                        echo "</div>";
                    echo form_close();
                ?>
            </div>
        </div>
    </div>
</main>
<script src="<?php echo base_url('assets/js/JqueryValidation/jquery.js')?>"></script>
<script src="<?php echo base_url('assets/js/JqueryValidation/jquery.validate.min.js')?>"></script>
<script src="<?php echo base_url('assets/js/JqueryValidation/additional-methods.min.js')?>"></script>
