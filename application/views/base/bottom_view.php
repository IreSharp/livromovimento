        </main>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    
    <script>window.jQuery || document.write('<script src="../../../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script type="text/javascript" src="<?php echo base_url('assets/bootstrap/js/popper.min.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/activeMenu.js')?>"></script>

    <!-- Import Jquery -->
    <script type="text/javascript" src="<?php echo base_url('assets/js/jquery-validation-1.17.0/lib/jquery-3.1.1.js')?>"></script>

    <!-- Import Bootstrap -->
    <script type="text/javascript" src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js')?>"></script>

    <!-- Imports dataTable -->
    <script type="text/javascript" src="<?php echo base_url('assets/dataTables/js/jquery.dataTables.min.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/dataTables/js/dataTables.rowReorder.min.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/dataTables/js/dataTables.responsive.min.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/dataTables/js/dataTables.min.js')?>"></script>

    <!-- Import JqueryValidate -->
    <script type="text/javascript" src="<?php echo base_url('assets/js/jquery-validation-1.17.0/dist/jquery.validate.min.js')?>"></script>

    <!-- Import JqueryMask -->
    <script type="text/javascript" src="<?php echo base_url('assets/js/jQuery-Mask-Plugin-master/dist/jquery.mask.min.js')?>"></script>

    <!-- import HammerJs -->
    <script type="text/javascript" src="<?php echo base_url('node_modules/hammerjs/hammer.min.js')?>"></script>
    
  </body>
</html>
