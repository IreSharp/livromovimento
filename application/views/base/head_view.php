<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <!-- <meta http-equiv="refresh" content="2" /> -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<?php echo base_url()?>icon/favicon.ico">
    
    <link rel="shortcut icon" href="<?php echo base_url()?>icon/favicon.ico" type="image/x-icon">
    <link rel="icon" href="<?php echo base_url()?>icon/favicon.ico" type="image/x-icon">

    <title>Livro em Movimento</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url('assets/bootstrap/css/bootstrap.css');?>" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url('assets/bootstrap/css/dashboard.css')?>" rel="stylesheet">
    
    <!--font awesome-->
    <link rel="stylesheet" href="<?php echo base_url('assets/font-awesome/css/font-awesome.min.css')?> "> 
    
    <!--    dataTable Import    -->
    <link rel="stylesheet" href="<?php echo base_url('assets/dataTables/css/dataTables.rowReorder.min.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/dataTables/css/dataTables.bootstrap4.min.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/dataTables/css/dataTables.responsive.min.css');?>">
    
    <!--    css personalizado   -->
    <link rel="stylesheet" href="<?php echo base_url('assets/css/personalizado.css');?>">

    <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.min.js')?>"></script>
    
    
  </head>