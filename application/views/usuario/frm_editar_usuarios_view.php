<main role="main" class="col-sm-9 ml-sm-auto col-md-10 pt-3">
    <div class="container">
        <div class="row">
            <div class="col-10 ">
                <div class="col-12 pageTitle">
                    <legend><a class="fa fa-arrow-left" href="<?php echo base_url("usuario")?>" ></a>&nbsp<?php echo $tituloPagina?><a id="salvaEdicao" class="fa fa-floppy-o pull-right" href="#"></a></legend>
                </div>
                <?php
                    echo $this->session->flashdata('statusOperacao');
                    $data = ['id'=>'formularioCadastro'];
                    echo form_open('usuario/salvaEdusu',$data);
                        echo "<div class='row'>";
                            echo "<div class='col-3'>";
                                echo "<input type='hidden' name='id' value=".$dadosUsuario['id'].">";
                                echo form_label("Nome","nome");
                                $op = ['name'=>'nome','value'=>$dadosUsuario['nome'],'placeholder'=>"Nome",'class'=>"form-control",'id'=>'nome','required'=>''];
                                echo form_input($op);
                            echo "</div>";
                            echo "<div class='col-3'>";
                                echo form_label("Telefone","telefone");
                                $op = ['name'=>'telefone','value'=>$dadosUsuario['telefone'],'placeholder'=>'Telefone','class'=>"form-control",'id'=>'telefone','required'=>''];
                                echo form_input($op);
                            echo "</div>";
                            echo "<div class='col-3'>";
                                $op = [""=>""];
                                foreach($formacoes as $formacao){
                                    $op[ $formacao['id'] ] = $formacao['nome'];
                                }
                                echo form_label("Formacao","formacao_id");
                                echo form_dropdown('formacao_id',$op,$dadosUsuario['formacao_id'],'class="form-control" id="formacaoSelect" required');
                            echo "</div>";
                            echo "<div class='col-3' id='areaAtuacao'>";
                                echo form_label("Curso","area_de_atuacao");
                                $op = ['name'=>'area_de_atuacao','value'=>$dadosUsuario['area_de_atuacao'],'id'=>'cursoId','placeholder'=>'Curso','class'=>"form-control"];
                                echo form_input($op);
                            echo "</div>";
                            echo "<div class='col-3'>";
                                echo '<input type="submit" value="Submit">';
                            echo "</div>";
                        echo "</div>";
                    echo form_close();
                ?>
            </div>
        </div>
    </div>
</main>
<script src="<?php echo base_url('assets/js/usuario/cadastro.js')?>"></script>
<script src="<?php echo base_url('assets/js/JqueryValidation/jquery.js')?>"></script>
<script src="<?php echo base_url('assets/js/JqueryValidation/jquery.validate.min.js')?>"></script>
<script src="<?php echo base_url('assets/js/JqueryValidation/additional-methods.min.js')?>"></script>

