<main role="main">

    <div class="row">
        <div class="col-sm-1 full-screen-size" id="voltar">
            <span class="fa fa-chevron-left back-icon"></span>
        </div>

        <div class="col-sm-10 full-screen-size pt-3">
            <div class="col-12 pageTitle">
                <legend><?php echo $tituloPagina?><a id="salvaUsuario" class="fa fa-floppy-o pull-right" href="#"></a></legend>
            </div>
            <?php
                echo $this->session->flashdata('statusOperacao');
                $data = ['id'=>'formularioCadastro'];
                echo form_open('usuario/salvaUsuario',$data);
                    echo "<div class='col-md-10'>";
                        echo "<div class='row'>";
                            echo "<div class='col-md-3'>";
                                echo form_label("Nome","nome");
                                $op = ['name'=>'nome','value'=>$dadosUsuario['nome'],'placeholder'=>"Nome",'class'=>"form-control",'id'=>'nome'];
                                echo form_input($op);
                            echo "</div>";
                            echo "<div class='col-md-3'>";
                                echo form_label("Telefone","telefone");
                                $op = ['name'=>'telefone','value'=>$dadosUsuario['telefone'],'placeholder'=>'Telefone','class'=>"form-control",'id'=>'telefone'];
                                echo form_input($op);
                            echo "</div>";
                            echo "<div class='col-md-3'>";
                                $op = [""=>""];
                                foreach($formacoes as $formacao){
                                    $op[ $formacao['id'] ] = $formacao['nome'];
                                }
                                echo form_label("Formacao","formacao_id");
                                echo form_dropdown('formacao_id',$op,$dadosUsuario['formacao_id'],'class="form-control" id="formacaoSelect"');
                            echo "</div>";
                            echo "<div class='col-md-3' id='areaAtuacao'>";
                                echo form_label("Curso","area_de_atuacao");
                                $op = ['name'=>'area_de_atuacao','value'=>$dadosUsuario['area_de_atuacao'],'id'=>'cursoId','placeholder'=>'Curso','class'=>"form-control capitalize"];
                                echo form_input($op);
                            echo "</div>";
                        echo "</div>";
                        echo "<div class='row pt-3'>";
                            echo "<div class='col-md-3'>";
                                echo form_label("Areas de Interesse","outros");
                                $op = ['name'=>'outros','value'=>$dadosUsuario['outros'],'id'=>'outros','placeholder'=>'Areas de interesse ','class'=>"form-control capitalize"];
                                echo form_input($op);
                            echo "</div>";
                        echo "</div>";
                    echo "</div>";
                echo form_close();
            ?>
        </div>
        
    </div>
    
    
</main>

<script type="text/javascript" src="<?php echo base_url('assets/js/usuario/cadastro.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery-validation-1.17.0/dist/jquery.validate.min.js')?>"></script>