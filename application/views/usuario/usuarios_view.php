<main role="main" id="mainContent" >
    <div class="row pt-3">
        <div class="col-md-10 offset-md-1" >
            <div class="col-md-12 pageTitle">
                <legend >Usuarios <a class="fa fa-plus pull-right" id="novoUsuario" href="usuario/novo"></a></legend>
            </div>
            <div class="row">
                <div class="col-md-10 offset-md-1" >
                    <table class="table table-striped table-bordered" id="tabelaUsuario">
                        <thead>
                            <tr>
                                <th id="nome">Nome</th>
                                <th>Telefone</th>
                                <th>Formacao</th>
                                <th>Curso</th>
                                <th>Acoes</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                foreach($usuarios as $usuario){
                                    echo "<tr>".
                                            "<td>".$usuario['nome']."</td>".
                                            "<td>".$usuario['telefone']."</td>".
                                            "<td>".$usuario['formacao_id']."</td>".
                                            "<td>".$usuario['area_de_atuacao']."</td>".
                                            "<td>
                                                <a class='fa fa-pencil iconeEdicao' href='usuario/edita/".$usuario['id']."'></a>
                                                <a class='fa fa-times iconeExclusao' href='usuario/desabilita/".$usuario['id']."'></a>
                                            </td>".
                                        "</tr>";
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</main>
<script src="<?php echo base_url('assets/js/usuario/home.js')?>"></script>