<?php

class M_Pedidos extends CI_Model{
    
    const tabelaPedido = "pedido";

    function __construct() {
        parent::__construct();
    }
    
    //Select 

    public function retornaPedidos(){

        return $this->db->get(self::tabelaPedido)->result_array();
       //  $tes = $this->db->get(self::tabelaPedido)->result_array();
       //  print_r($tes);
       //  exit();
    }
    public function retornaPedido($id){
       $this->db->where("id",$id);
        return $this->db->get(self::tabelaPedido)->row_array();
    }
    
    public function retornaIdNovoPedido(){
        $this->db->select_max("id");
        $te = $this->db->get('pedido')->row_array();
        return $te['id'] + 1;
    }
    public function retornaUltimoPedido(){
        $this->db->select_max("id");
        return $this->db->get('pedido')->row_array();
    }

    public function salvaPedido($data){
        return $this->db->insert(self::tabelaPedido, $data);
    }
    
    public function salvaEd($id,$data){
        $this->db->where("id", $id);  
         return $this->db->update("pedido", $data);
         return 1;

   }
}