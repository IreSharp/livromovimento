<?php

class M_livros extends CI_Model{
    
    const tabelaLivro = "livro";

    function __construct() {
        parent::__construct();
    }
    
    //Select 

    public function retornaLivros(){

        return $this->db->get(self::tabelaLivro)->result_array();
     //    $tes = $this->db->get(self::tabelaLivro)->result_array();
        // print_r($tes);
        // exit();
    }
    public function retornaLivro($id){
        $this->db->where("id",$id);
        return $this->db->get(self::tabelaLivro)->row_array();
    }
    
    public function retornaIdNovoLivro(){
        $this->db->select_max("id");
        $te = $this->db->get('livro')->row_array();
        return $te['id'] + 1;
    }
    public function retornaUltimoLivro(){
        $this->db->select_max("id");
        return $this->db->get('livro')->row_array();
    }

    public function salvaLivro($data){
        return $this->db->insert(self::tabelaLivro, $data);
    }
    
    public function salvaEd($id,$data){
        $this->db->where("id", $id);  
         return $this->db->update("livro", $data);
         return 1;

   }
}