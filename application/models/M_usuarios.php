<?php

class M_usuarios extends CI_Model{
    
    const tabelaUsuario = "usuario";
    const tabelaFormacao = "formacao";

    function __construct() {
        parent::__construct();
    }
    
    //Select 

    public function retornaUsuarios(){

        $this->db->select("usuario.id,usuario.nome,telefone,formacao.nome as formacao_id,area_de_atuacao, usuario.status");
        $this->db->where('status','A');
        $this->db->join('livroMovimento.formacao','livroMovimento.usuario.formacao_id = livroMovimento.formacao.id','left');
        return $this->db->get(self::tabelaUsuario)->result_array();
    }
    
    public function retornaUsuario($id){
        $this->db->where("id",$id);
        return $this->db->get(self::tabelaUsuario)->row_array();
    }
    
    public function desabilitaUsuario($id){
        // $this->db->where("status",$status);
        // $status = $this->db->query("select status from usuario   where id = status")->row()->status; 
        // if ($status== A)
        // {
        //     $status = I;
        // } 
        // else
        // {
        //     $status = A;
        // }
        
        // $data=array('status'=>$status);
        // return $this->db->update("usuario", $data);
        $this->db->where('id',$id); 
        return $this->db->update(self::tabelaUsuario, ['status' => 'I']);

    }
    
    public function retornaFormacao(){
        return $this->db->get("formacao")->result_array();
    }

    public function retornaIdNovoUsuario(){
        $this->db->select_max("id");
        $te = $this->db->get('usuario')->row_array();
        return $te['id'] + 1;
    }

    public function retornaUltimoId(){
        $this->db->select_max("id");
        return $this->db->get('usuario')->row_array();
    }

    // Insert

    public function salvaUsuario($data){
        return $this->db->insert(self::tabelaUsuario, $data);
    }

    

    public function salvaEd($id,$data){
        $this->db->where("id", $id);  
         return $this->db->update("usuario", $data);
         return 1;

    }
}