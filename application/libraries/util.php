<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
error_reporting(0);
setlocale(LC_ALL, 'pt_BR.UTF-8');
class Util{
    
    public function __construct(){
        $this->CI =& get_instance();
        
    }
    
    public function imprimeVetor($vetor){   
        echo '<pre>';
        print_r($vetor);
        echo '</pre>';
        exit();
    }



    /*  

        *   @Autor: Romulo Lobosco
        *   @Version: 1.0
        *   @Data_Criacao: 11/02/2018
        *
        *
        *
        *
        *   Prepara as informacoes vindas da tela de usuario para serem salvas no banco de dados
        *   deve ser utilizado apenas para este fim.
        *
        *   As informacoes da tela de usuario estao dispostas da seguinte maneira:
        *        [nome] => nome
        *        [telefone] => 999999999
        *        [formacao] => 2 -> chave estrangeira para tabela formacao
        *        [area_de_atuacao] => teste
    */
    public function formataUsuario($data){
        return $data;
    }
}