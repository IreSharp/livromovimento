<?php

class livro extends CI_Controller{
   
    function __construct() {
        
        parent::__construct();
        $this->load->model('M_livros','mLivro');
        $this->load->library('Util','','util');
    }
    
    public function index(){
        
        $dados['livros'] = $this->mLivro->retornaLivros();

        // $this->util->imprimeVetor($dados);

        $this->load->view('base/head_view');
        $this->load->view('base/menuSuperior_view');
        $this->load->view('livro/livros_view', $dados);
        $this->load->view('base/bottom_view');
    }

    public function cadastraLivro(){
        
         $dados['tituloPagina'] = "Cadastrar Livro";

        $this->load->view('base/head_view');
        $this->load->view('base/menuSuperior_view');
        $this->load->view('livro/frm_livro_view',$dados);
        $this->load->view('base/bottom_view');
    }
    
    public function editaLivro($id){
        
        $dados['tituloPagina'] = "Edita Livro";
        $dados['dadosLivro'] = $this->mLivro->retornaLivro($id);
        
        
        $this->load->view('base/head_view');
        $this->load->view('base/menuSuperior_view');
        $this->load->view('livro/frm_livro_view',$dados);
        $this->load->view('base/bottom_view');
        
    }

    
   public function editarLivro($id){

    $dados['tituloPagina'] = "Editar Livro";
    $dados['dadosLivro'] = $this->mLivro->retornaLivro($id);
    
    // echo $this->util->imprimeVetor( $dados );

    $this->load->view('base/head_view');
    $this->load->view('base/menuSuperior_view');    
    $this->load->view('livro/frm_editar_livro_view',$dados);
    $this->load->view('base/bottom_view');
    
}
       
    public function salvaLivro(){
        
            $dados = $_POST;
    
            if( empty( $dados['nome']&&$dados['autor']&&$dados['ano']&&$dados['tema'] ) ){
                $status = false;
                $erro = " - todos os campos devem ser preenchidos";
            }else{
                $status = $this->mLivro->salvaLivro( $dados );
                $id_livro = $this->mLivro->retornaUltimoLivro();
                // print_r($id_livro);
                // exit();
            }
    
            if($status){
                $this->session->set_flashdata('statusOperacao', '<div class="alert alert-success"><strong>Livro salvo com sucesso!</strong></div>');
                redirect(base_url('livro/editarLivro/'.$id_livro['id']));
            }else{
                $this->session->set_flashdata('statusOperacao', '<div class="alert alert-danger">Erro ao salvar usu&aacute;rio'.$erro.'</div>');
                redirect(base_url('livro/cadastraLivro'));
            }
        }            
            public function salvaEdliv(){

                $id = array_shift($_POST);
                $dados = $_POST;
                
                
                if( empty( $dados['nome']&&$dados['autor']&&$dados['ano']&&$dados['tema'] ) ){
                    $status = false;
                    $erro = " - todos os campos devem ser preenchidos";
                }else{
                    $status = $this->mLivro->salvaEd( $id, $dados );
                    $id_livro = $id;
                    // print_r($status);
                    //exit();
                }
                
                // exit();
    
                if($status){
                    $this->session->set_flashdata('statusOperacao', '<div class="alert alert-success"><strong>Livro salvo com sucesso!</strong></div>');
                    redirect(base_url('livro/editarLivro/'.$id_livro['id']));
                }else{
                    $this->session->set_flashdata('statusOperacao', '<div class="alert alert-danger">Erro ao salvar livro'.$erro.'</div>');
                    redirect(base_url('livro/cadastraLivro'));
        
        
                }

        } 
}