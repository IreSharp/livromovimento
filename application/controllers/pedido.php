<?php

class pedido extends CI_Controller{
    
    function __construct() {
        
        parent::__construct();
        $this->load->model('M_pedidos','mPedido');
        $this->load->library('Util','','util');
    }
    
    function index(){

        $dados['pedidos'] = $this->mPedido->retornaPedidos();


        $this->load->view('base/head_view');
        $this->load->view('base/menuSuperior_view');
        $this->load->view('pedido/pedidos_view', $dados);
        $this->load->view('base/bottom_view');
    }

    public function cadastraPedido(){
        
        $dados['tituloPagina'] = "Cadastrar Pedido";

        $this->load->view('base/head_view');
        $this->load->view('base/menuSuperior_view');
        $this->load->view('pedido/frm_pedido_view',$dados);
        $this->load->view('base/bottom_view');
    }
    public function editaPedido($id){
        
        $dados['tituloPagina'] = "Edita Pedido";
        $dados['dadosPedido'] = $this->mPedido->retornaPedido($id);
        
        
        $this->load->view('base/head_view');
        $this->load->view('base/menuSuperior_view');
        $this->load->view('pedido/frm_pedido_view',$dados);
        $this->load->view('base/bottom_view');
        
    }

    
    public function editarPedido($id){

    $dados['tituloPagina'] = "Editar Pedido";
    $dados['dadosPedido'] = $this->mPedido->retornaPedido($id);
    
    // echo $this->util->imprimeVetor( $dados );

    $this->load->view('base/head_view');
    $this->load->view('base/menuSuperior_view');    
    $this->load->view('pedido/frm_editar_pedido_view',$dados);
    $this->load->view('base/bottom_view');
    
}
       
    public function salvaPedido(){
        
            $dados = $_POST;
    
            if( empty( $dados['usuario']&&$dados['livro'] ) ){
                $status = false;
                $erro = " - todos os campos devem ser preenchidos";
            }else{
                $status = $this->mPedido->salvaPedido( $dados );
                $id_pedido = $this->mPedido->retornaUltimoPedido();
                // print_r($id_livro);
                // exit();
            }
    
            if($status){
                $this->session->set_flashdata('statusOperacao', '<div class="alert alert-success"><strong>Pedido salvo com sucesso!</strong></div>');
                redirect(base_url('pedido/editarPedido/'.$id_pedido['id']));
            }else{
                $this->session->set_flashdata('statusOperacao', '<div class="alert alert-danger">Erro ao salvar usu&aacute;rio'.$erro.'</div>');
                redirect(base_url('pedido/cadastraPedido'));
            }
        }            
            public function salvaEdPedi(){

                $id = array_shift($_POST);
                $dados = $_POST;
                
                
                if( empty( $dados['Usuario']&&$dados['Livro'] ) ){
                    $status = false;
                    $erro = " - todos os campos devem ser preenchidos";
                }else{
                    $status = $this->mPedido->salvaEd( $id, $dados );
                    $id_pedido = $id;
                    // print_r($status);
                    //exit();
                }
                
                // exit();
    
                if($status){
                    $this->session->set_flashdata('statusOperacao', '<div class="alert alert-success"><strong>Pedido salvo com sucesso!</strong></div>');
                    redirect(base_url('pedido/editarPedido/'.$id_pedido['id']));
                }else{
                    $this->session->set_flashdata('statusOperacao', '<div class="alert alert-danger">Erro ao salvar Pedido'.$erro.'</div>');
                    redirect(base_url('pedido/cadastraPedido'));
        
        
                }
       }
}