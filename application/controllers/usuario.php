<?php

class usuario extends CI_Controller{

    function __construct() {
        parent::__construct();
        
        $this->load->model('M_usuarios','usuModel');
        $this->load->library('Util','','util');
        
    }
    
    public function index(){
        
        $dados['usuarios'] = $this->usuModel->retornaUsuarios();

        // echo $this->util->imprimeVetor( $dados );

        $this->load->view('base/head_view');
        $this->load->view('base/menuSuperior_view');
        $this->load->view('usuario/usuarios_view',$dados);
        $this->load->view('base/bottom_view');
    }
    public function novo(){
        
        $dados['tituloPagina'] = "Cadastrar Usuarios";
        $dados['formacoes'] = $this->usuModel->retornaFormacao();

        // echo $this->util->imprimeVetor( $dados );

        $this->load->view('base/head_view');
        $this->load->view('base/menuSuperior_view');
        $this->load->view('usuario/frm_usuarios_view',$dados);
        $this->load->view('base/bottom_view');
    }
    
    public function edita($id){
        
        $dados['tituloPagina'] = "Editar Usuarios";
        $dados['dadosUsuario'] = $this->usuModel->retornaUsuario($id);
        $dados['formacoes'] = $this->usuModel->retornaFormacao();
        
        // echo $this->util->imprimeVetor( $dados );

        $this->load->view('base/head_view');
        $this->load->view('base/menuSuperior_view');
        $this->load->view('usuario/frm_usuarios_view',$dados);
        $this->load->view('base/bottom_view');
        
    }
    
    public function salvaUsuario(){

        // echo ("pao");
        // exit();
        
        $dados = $_POST;
         
        
        //$this->util->imprimeVetor($dados);


        
            $status = $this->usuModel->salvaUsuario( $dados );
            $id_usuario = $this->usuModel->retornaUltimoId();
            // print_r($id_usuario);
            // exit();
        

        if($status){
            $this->session->set_flashdata('statusOperacao', '<div class="alert alert-success"><strong>Usuario salvo com sucesso!</strong></div>');
            redirect(base_url('usuario/edita/'.$id_usuario['id']));
        }else{
            $this->session->set_flashdata('statusOperacao', '<div class="alert alert-danger">Erro ao salvar usuario'.$erro.'</div>');
            redirect(base_url('usuario/cadastraUsuario'));
        }
        
    }
        public function editarUsuario($id){
        
            $dados['tituloPagina'] = "Editar Usuarios";
            $dados['dadosUsuario'] = $this->usuModel->retornaUsuario($id);
            $dados['formacoes'] = $this->usuModel->retornaFormacao();
            
            // echo $this->util->imprimeVetor( $dados );
    
            $this->load->view('base/head_view');
            $this->load->view('base/menuSuperior_view');
            $this->load->view('usuario/frm_editar_usuarios_view',$dados);
            $this->load->view('base/bottom_view');
            
        }
        public function salvaEdusu(){

            $id = array_shift($_POST);
            $dados = $_POST;
            
            
            if( empty( $dados['nome']&&$dados['telefone']&&$dados['formacao_id'] ) ){
                $status = false;
                $erro = " - todos os campos devem ser preenchidos";
            }else{
                $status = $this->usuModel->salvaEd( $id, $dados );
                $id_usuario = $id;
                // print_r($status);
                //exit();
            }
            
            // exit();

            if($status){
                $this->session->set_flashdata('statusOperacao', '<div class="alert alert-success"><strong>Usuario salvo com sucesso!</strong></div>');
                redirect(base_url('usuario/editarUsuario/'.$id_usuario['id']));
            }else{
                $this->session->set_flashdata('statusOperacao', '<div class="alert alert-danger">Erro ao salvar usuario'.$erro.'</div>');
                redirect(base_url('usuario/cadastraUsuario'));
    
    
            }
        }
        Public function desabilita($id){
        
            $dados['tituloPagina'] = "Editar Usuarios";
            $dados['desabilita'] = $this->usuModel->desabilitaUsuario($id);
            
            
            // echo $this->util->imprimeVetor( $dados );
    
            $this->load->view('base/head_view');
            $this->load->view('base/menuSuperior_view');
            $this->load->view('usuario/frm_usuarios_view',$dados);
            $this->load->view('base/bottom_view');
            redirect('usuario');
        }

    
    }
