<?php

class pagamento extends CI_Controller{
    
    function __construct() {
        
        parent::__construct();
        $this->load->library('Util','','util');
    }
    
    function index(){

        // redirect(base_url('APagamento/teste'));

        $this->load->view('base/head_view');
        $this->load->view('base/menuSuperior_view');
        $this->load->view('pagamento/pagamentos_view');
        $this->load->view('base/bottom_view');
    }

    public function cadastraPagamento(){
        
        $dados['tituloPagina'] = "Cadastrar Pagamento";

        $this->load->view('base/head_view');
        $this->load->view('base/menuSuperior_view');
        $this->load->view('pagamento/frm_pagamento_view',$dados);
        $this->load->view('base/bottom_view');
    }
}